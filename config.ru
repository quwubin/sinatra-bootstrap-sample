# encoding: UTF-8

require 'sinatra'
require './app.rb'
set :environment, :development
run Sinatra::Application
