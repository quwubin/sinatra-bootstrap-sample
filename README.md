About sinatra-bootstrap-sample
====

This is a demonstrator for [Sinatra](http://www.sinatrarb.com/), [Twitter Bootstrap](http://twitter.github.com/bootstrap/), [sinatra-url-for](https://github.com/emk/sinatra-url-for) and [sinatra-static-assets](https://github.com/wbzyl/sinatra-static-assets).

Use it as a boilerplate to start your own apps.

Inspired by [pokle](https://github.com/pokle/sinatra-bootstrap), thanks.

Go!
===

Install dependencies:

```

  rvm 1.9.3
  gem install sinatra
  gem install emk-sinatra-url-for -s http://gems.github.com
  gem install sinatra-static-assets -s http://gemcutter.org

```
Download and run sinatra-bootstrap-sample:

```
  git clone git@github.com:quwubin/sinatra-bootstrap-sample.git
  cd sinatra-bootstrap-sample
  ruby app.rb
```

Then open [http://localhost:4567/](http://localhost:4567/)
