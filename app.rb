require 'sinatra'
require 'sinatra/url_for'
require 'sinatra/static_assets'

USERS = {
  quwb: 'quwb000',
  admin: 'admin000'
}

configure do
  enable :sessions, :dump_errors, :raise_errors
end

helpers do
  def login?
    session[:identity]
  end
  def username
    session[:identity] ? session[:identity] : false
  end
end

before do
  @year = Time.now.year
end


get '/' do
  erb :index
end

get '/login' do
  erb :login_form
end

post '/login/attempt' do
  username = params['username']
  if USERS[username.to_sym] == params['password']
    session[:identity] = username
  end
  where_user_came_from = session[:previous_url] || '/'
  redirect to where_user_came_from
end

get '/logout' do
  session.delete(:identity)
  redirect to url_for "/"
end
